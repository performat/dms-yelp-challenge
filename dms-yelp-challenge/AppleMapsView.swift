import MapKit

class AppleMapsView: MapView, MKMapViewDelegate {
    weak var delegate: MapViewDelegate?
    var models: [AnnotationModel] { return annotations.map { $0.model }}
    private var annotations: [AppleMapsAnnotation] { return mapView.annotations.compactMap { $0 as? AppleMapsAnnotation} }
    var region: MKCoordinateRegion { return mapView.region }
    var selectedModel: AnnotationModel? {
        get {
            return selectedAnnotations.first?.model
        }
        
        set {
            guard newValue?.id != selectedModel?.id else { return }
            
            if let current: AnnotationModel = self.selectedModel {
                guard let annotation: AppleMapsAnnotation = self.selectedAnnotations.first(where: { return current == $0.model }) else { return }
                self.mapView.deselectAnnotation(annotation, animated: true)
            }
            
            if let model: AnnotationModel = newValue {
                guard let annotation: AppleMapsAnnotation = self.annotations.first(where: { model == $0.model }) else { return }
                self.mapView.selectAnnotation(annotation, animated: true)
            }
        }
    }
    var showsUserLocation: Bool {
        set { mapView.showsUserLocation = newValue }
        get { return mapView.showsUserLocation }
    }
    
    private let mapView: MKMapView = MKMapView()
    
    private var selectedAnnotations: [AppleMapsAnnotation] {
        return mapView.selectedAnnotations.compactMap { $0 as? AppleMapsAnnotation }
    }
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        mapView.delegate = self
        mapView.isRotateEnabled = false
        mapView.userLocation.title = nil
        mapView.userLocation.subtitle = nil
        
        mapView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(mapView)
        
        NSLayoutConstraint.activate([mapView.leadingAnchor.constraint(equalTo: leadingAnchor),
                                     mapView.trailingAnchor.constraint(equalTo: trailingAnchor),
                                     mapView.bottomAnchor.constraint(equalTo: bottomAnchor),
                                     mapView.topAnchor.constraint(equalTo: topAnchor)])
        
        if #available(iOS 11.0, *) {
            mapView.register(MKAnnotationView.self, forAnnotationViewWithReuseIdentifier: AppleMapsAnnotation.identifier)
        }
    }
    
    func add(models: [AnnotationModel]) {
        let annotations: [AppleMapsAnnotation] = models.map { AppleMapsAnnotation(model: $0) }
        mapView.addAnnotations(annotations)
    }
    
    func remove(models: [AnnotationModel]) {
        let remove: [MKAnnotation] = annotations.filter { (annotation: AppleMapsAnnotation) -> Bool in
            return models.contains(where: { (model: AnnotationModel) -> Bool in return model == annotation.model })
        }
        mapView.removeAnnotations(remove)
    }
    
    func update(models: [AnnotationModel]) {
        annotations.forEach { (annotation: AppleMapsAnnotation) -> Void in
            guard let model: AnnotationModel = models.first(where: { (model: AnnotationModel) -> Bool in return model == annotation.model }) else { return }
            annotation.model = model
        }
    }
    
    // MARK: - Region
    
    func center(on region: MKCoordinateRegion, insets: UIEdgeInsets, animated: Bool) {
        mapView.setVisibleMapRect(region.mkMapRect, edgePadding: insets, animated: animated)
    }
    
    // MARK: MKMapViewDelegate
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        delegate?.mapView(self, regionDidChange: region)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation: AppleMapsAnnotation = view.annotation as? AppleMapsAnnotation else { return }
        self.delegate?.mapView(self, didSelectAnnotation: annotation.model)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        guard let annotation: AppleMapsAnnotation = view.annotation as? AppleMapsAnnotation else { return }
        self.delegate?.mapView(self, didDeselectAnnotation: annotation.model)
    }
}
