import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController, MapViewDelegate, CLLocationManagerDelegate, SearchViewControllerDelegate {
    var items: [Item] = []
    
    private let mapView: MapView = AppleMapsView()
    private let searchVC: SearchViewController = SearchViewController()
    private let locationManager: CLLocationManager = CLLocationManager()
    private let centerSpan: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.003, longitudeDelta: 0.003)
    private var mapInsets: UIEdgeInsets {
        return UIEdgeInsets(top: 50.0, left: 50.0, bottom: searchVC.view.frame.height, right: 50.0)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        } else if CLLocationManager.authorizationStatus() == .denied {
            // show an alert vc
        } else {
            locationManager.startUpdatingLocation()
        }
        
        mapView.delegate = self
        mapView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(mapView)
        
        NSLayoutConstraint.activate([
            mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mapView.topAnchor.constraint(equalTo: view.topAnchor),
            mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        mapView.showsUserLocation = true
        
        if let userLocation: CLLocation = CLLocationManager().location {
            let region: MKCoordinateRegion = MKCoordinateRegion(center: userLocation.coordinate, span: centerSpan)
            // Ensure we have the proper map insets before centering the map
            view.setNeedsLayout()
            view.layoutIfNeeded()

            mapView.center(on: region, insets: mapInsets, animated: false)
        }
        
        searchVC.delegate = self
        addChildWithView(searchVC)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        guard let keyboardFrameEnd: CGRect = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else { return }
        
        let yPos: CGFloat =
            view.bounds.maxY
            - keyboardFrameEnd.height
            - searchVC.view.bounds.midY
            + view.safeAreaInsetsIfAvailable.bottom
        searchVC.view.center = CGPoint(x: view.bounds.midX, y: yPos)
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        searchVC.view.center = CGPoint(x: view.bounds.midX,
                                          y: view.bounds.maxY - searchVC.view.bounds.midY)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        searchVC.view.setNeedsLayout()
        searchVC.view.layoutIfNeeded()
        searchVC.view.bounds.size = searchVC.preferredContentSize
        searchVC.view.center = CGPoint(x: view.bounds.midX,
                                          y: view.bounds.maxY - searchVC.view.bounds.midY)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch: UITouch = touches.first else { return }
        let location: CGPoint = touch.location(in: searchVC.view)
        let isTouchInSearchVC: Bool = searchVC.view.point(inside: location, with: event)
        
        if !isTouchInSearchVC {
            _ = searchVC.resignFirstResponder()
        }
    }
    
    // CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // TODO: Show an alert VC
    }
    
    // MARK: MapViewDelegate
    
    func mapView(_ mapView: Map, didSelectAnnotation model: AnnotationModel) {
        guard let business: Business = model as? Business else { return }
        searchVC.select(business: business)
    }
    
    func mapView(_ mapView: Map, didDeselectAnnotation model: AnnotationModel) {
        guard let business: Business = model as? Business else { return }
        searchVC.deselect(business: business)
    }
    
    func mapView(_ mapView: Map, regionDidChange region: MKCoordinateRegion) {
        // TODO: Offer to search again for results in this map region
    }
    
    func mapView(_ mapView: Map, regionIsChanging region: MKCoordinateRegion) {}
    
    // MARK: SearchViewControllerDelegate
    
    func searchViewControllerDidFind(items: [Item]) {
        mapView.remove(models: self.items) // TODO: Optimize and do a diff
        mapView.add(models: items)
        self.items = items
    }
    
    func searchViewControllerDidSelect(item: Item) {
        mapView.selectedModel = item
        let region: MKCoordinateRegion = MKCoordinateRegion(center: item.coordinate, span: centerSpan)
        mapView.center(on: region, insets: mapInsets, animated: true)
    }
}

