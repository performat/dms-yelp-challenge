import Foundation

protocol AnnotationAdapter {
    associatedtype T
    associatedtype Y
    mutating func adapt(annotations: [T], for mapView: Y)
    func adapt(annotation: T, for mapView: Y)
}
