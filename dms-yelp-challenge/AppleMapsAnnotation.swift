import MapKit

class AppleMapsAnnotation: NSObject, MKAnnotation {
    static let identifier: String = String(describing: type(of: self))
    private(set) var coordinate: CLLocationCoordinate2D
    var model: AnnotationModel {
        didSet {
            coordinate = model.coordinate
        }
    }
    
    init(model: AnnotationModel) {
        self.model = model
        self.coordinate = model.coordinate
    }
}
