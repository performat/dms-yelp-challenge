import Foundation

enum Result<V, E> {
    case success(V)
    case failure(E)
}
