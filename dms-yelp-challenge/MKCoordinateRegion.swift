import MapKit

extension MKCoordinateRegion {
    var mkMapRect: MKMapRect {
        let northWestPoint: MKMapPoint = MKMapPoint(northWest)
        let southEastPoint: MKMapPoint = MKMapPoint(southEast)
        
        let origin: MKMapPoint = MKMapPoint(x: min(northWestPoint.x, southEastPoint.x), y: min(northWestPoint.y, southEastPoint.y))
        let size: MKMapSize = MKMapSize(width: abs(northWestPoint.x-southEastPoint.x), height: abs(northWestPoint.y-southEastPoint.y))
        
        return MKMapRect(origin: origin, size: size)
    }
    
    var northEast: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: center.latitude - span.latitudeDelta/2,
                                      longitude: center.longitude - span.longitudeDelta/2)
    }
    
    var southWest: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: center.latitude + span.latitudeDelta/2,
                                      longitude: center.longitude + span.longitudeDelta/2)
    }
    
    var northWest: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: center.latitude + span.latitudeDelta/2,
                                      longitude: center.longitude - span.longitudeDelta/2)
    }
    
    var southEast: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: center.latitude - span.latitudeDelta/2,
                                      longitude: center.longitude + span.longitudeDelta/2)
    }
}
