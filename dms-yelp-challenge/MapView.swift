import MapKit

typealias MapView = UIView & Map

protocol MapViewDelegate: class {
    func mapView(_ mapView: Map, didSelectAnnotation model: AnnotationModel)
    func mapView(_ mapView: Map, didDeselectAnnotation model: AnnotationModel)
    func mapView(_ mapView: Map, regionIsChanging region: MKCoordinateRegion)
    func mapView(_ mapView: Map, regionDidChange region: MKCoordinateRegion)
}

protocol Map: class {
    var delegate: MapViewDelegate? { get set }
    
    // User Location
    var showsUserLocation: Bool { get set }
    
    // Annotations
    var selectedModel: AnnotationModel? { get set }
    var models: [AnnotationModel] { get }
    func add(models: [AnnotationModel])
    func remove(models: [AnnotationModel])
    func update(models: [AnnotationModel])
    
    // Region
    var region: MKCoordinateRegion { get }
    func center(on region: MKCoordinateRegion, insets: UIEdgeInsets, animated: Bool)
}
